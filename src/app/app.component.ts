import { Component } from '@angular/core';

import { Pelicula } from './interfaces/pelicula.interface';
import { peliculas } from './datos/peliculas';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private peliculas: Array<Pelicula> = peliculas;
  public titulo: string = "Mis pelis";
  public estrellas: Array<number> = [1, 2, 3, 4, 5];
  public formVisible: boolean = false;
  public nuevaPelicula: Pelicula = this.resetNuevaPelicula();

  public resetNuevaPelicula(): Pelicula {
    return {
      id: 0,
      titulo: "",
      director: "",
      anyo: 0,
      genero: "",
      puntuacion: 0
    };
  }

  public creaPelicula(): void {
    this.peliculas.push(Object.assign({}, this.nuevaPelicula));
    this.formVisible = false;
    this.nuevaPelicula = this.resetNuevaPelicula();
  }

  public debugPelicula(): string {
    return JSON.stringify(this.nuevaPelicula);
  }

  public toggleForm(): void {
    this.formVisible = !this.formVisible;
    this.nuevaPelicula = this.resetNuevaPelicula();
  }

  public nPeliculas(): number {
    return this.peliculas.length;
  }

  public comprobarPelicula(pelicula: Pelicula, estado: string): boolean {    
    if (estado == 'pendiente')
      return pelicula.puntuacion == 0;
    else
      return pelicula.puntuacion > 0;
  }

  public pelisPorEstado(estado: string): Array<Pelicula> {
    let pelisFiltradas: Array<Pelicula> = this.peliculas.filter(
      pelicula => this.comprobarPelicula(pelicula, estado) 
    );

    return pelisFiltradas;
  }

  public nPeliculasPorEstado(estado: string): number {
    let pelisFiltradas: Array<Pelicula> = this.pelisPorEstado(estado);
    return pelisFiltradas.length;    
  }

  public puntuarPelicula(pelicula: Pelicula, i: number) {
    pelicula.puntuacion = i;
  }

}
