export interface Pelicula {
    id: number;
    titulo: string;
    director: string;
    anyo: number;
    genero: string;
    puntuacion: number;
}