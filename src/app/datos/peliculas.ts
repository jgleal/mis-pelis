import { Pelicula } from '../interfaces/pelicula.interface';

export let peliculas: Array<Pelicula> = [
    {
        "id": 1,
        "titulo": "Titanic",
        "director": "Pepe Leon",
        "anyo": 1996,
        "genero": "Romántica",
        "puntuacion": 5
    },
    {
        "id": 2,
        "titulo": "La Historia Interminable",
        "director": "Luis Tosar",
        "anyo": 1978,
        "genero": "Aventura",
        "puntuacion": 4
    },
    {
        "id": 3,
        "titulo": "Lo que el viento se llevó",
        "director": "Manuel López",
        "anyo": 1967,
        "genero": "Drama",
        "puntuacion": 3
    },
    {
        "id": 4,
        "titulo": "King Kong",
        "director": "Laura Villar",
        "anyo": 1956,
        "genero": "Humor",
        "puntuacion": 0
    },
    {
        "id": 5,
        "titulo": "ET",
        "director": "María Valverde",
        "anyo": 1974,
        "genero": "Infantil",
        "puntuacion": 2
    },
    {
        "id": 6,
        "titulo": "Como agua para Chocolate",
        "director": "Jaime Ruiz",
        "anyo": 2010,
        "genero": "Thriller",
        "puntuacion": 0
    }
]